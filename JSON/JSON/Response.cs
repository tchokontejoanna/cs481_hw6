﻿using System;
using System.Collections.Generic;

namespace JSON
{
    public class Word
    {
        public string type { get; set; }
        public string definition { get; set; }
        public string example { get; set; }
    }

    public class Dictionnary
    {
        public List<Word> Dictionary { get; set; }
    }
}
