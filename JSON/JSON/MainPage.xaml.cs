﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace JSON
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
        }

        private async Task Request()
        {

            //Setting the proper url for the request

            string url = "https://owlbot.info/api/v2/dictionary/" + word.Text;

            // Initialize parameters for the request
            var request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Get;
            request.Headers.Add("Accept", "application/json");

            // Creation of the client
            var client = new HttpClient();

            // Checking Internet
            if (Connectivity.NetworkAccess == NetworkAccess.Internet)
            {
                // send the request and await for the response from the api 
                HttpResponseMessage response = await client.SendAsync(request);
                //Checking error after the response
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    HttpContent content = response.Content;
                    var json = await content.ReadAsStringAsync();
                    List<Word> res = JsonConvert.DeserializeObject<List<Word>>(json);
                    result.Text = json;
                    var listView = new ListView();
                    listView.ItemsSource = res;
                } else {
                    //Print message in case of error from the api
                    await DisplayAlert("Error", "Something ent wrong on the api", "Try with another word");
                }
            } else
            {
                await DisplayAlert("Error", "There is no connection avalaible", "Activate wifi");
            }
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            _ = Request();
        }
    }
}
